# DevOps Assessment

## Requirements

- Create a free AWS account. Please note that we may request access to your AWS account.
- Chef
- GIT

## General Instructions
1. Fork this repo as a public repo.
    ![Fork 1](instruction-images/1-fork-1.png)
    ![Fork 2](instruction-images/2-fork-2.png)
    ![Fork 3](instruction-images/3-fork-3.png)
1. Pull your repo locally.
1. Work on the 3 tasks below in a branch. Time limit is 2 hours.
1. When ready to submit, push the branch to your public repo.
1. Open a PR from the branch to the main branch.
    ![Create PR 1](instruction-images/4-create_pr-1.png)
    ![Create PR 2](instruction-images/5-create_pr-2.png)
    ![Create PR 3](instruction-images/6-create_pr-3.png)
1. Share the link to the public repo to us.

## Task #1
You will provision an AWS instance that will run Chef

- Provision a free t2.micro Ubuntu Instance in your AWS Account.
- Install Chef server in this instance

## Task #2
Create a Chef recipe that will do the following:

- Install Apache2 package
- Install Mysql package
- Create index.html in '/var/www/html/' folder
- index.html should contain "Hello [Your Name]"

Commit the recipe in GIT and run it in your AWS instance. Post the Public DNS or link to your server as a comment when you create the final Pull Request in your repo.

## Task #3
Create a shell script that takes 2 arguments and creates a sub-directory under ‘/var/www/html/’ and HTML file under that newly created sub-directory. The first argument should be used as the name of the sub-directory. The second argument should be used as the filename of the HTML file. The content of the HTML file should render “Welcome to Balsam Brands”.
Finally, run the script in your instance with arguments "balsam" and "index.html".

### We should be able to validate your work through GIT, http://[Your provided Public DNS]/ and http://[Your provided Public DNS]/balsam/. However, we will request access to your AWS account if we need to validate further.